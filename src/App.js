import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import {TiWeatherCloudy, TiWeatherDownpour, TiWeatherSnow, TiWeatherStormy, TiWeatherSunny, TiWeatherWindy, TiWeatherShower} from  'react-icons/ti';
const api = {
  key: "41b1c973b51718dedad6962bef1cd69d",
  base: "https://api.openweathermap.org/data/2.5/"
}

function App() {

  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});
  const [icon, setIcon] = useState(<TiWeatherSnow/>);
  // const [data, setData] = useState('');
  const weatherIcons = {
    Thunderstorm:<TiWeatherStormy/>,
    Drizzle:<TiWeatherShower/>,
    Rain:<TiWeatherDownpour/>,
    Snow:<TiWeatherSnow/>,
    Atmosphere: <TiWeatherWindy/>,
    Clear: <TiWeatherSunny/>,
    Clouds: <TiWeatherCloudy/>
  };

  const search = e => {
    if(e.key === "Enter"){
      fetch(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`)
      .then(res => res.json())
      .then(result => {
          setWeather(result);
          setQuery('');
          getWeatherIcon(result);
          console.log(result);
        });   
    }
  }
  const getWeatherIcon = (result) =>{
     let weatherId = result.weather[0].id;
    switch(true){
      case weatherId >= 200 && weatherId <=232 :
        setIcon(weatherIcons.Thunderstorm);
        break;
      case weatherId >= 300 && weatherId <= 321:
        setIcon(weatherIcons.Drizzle);
        break;
      case weatherId >= 500 && weatherId <= 521:
        setIcon(weatherIcons.Rain);
        break;
      case weatherId >= 600 && weatherId <= 622:
        setIcon(weatherIcons.Snow);
        break;
      case weatherId >= 701 && weatherId <= 781:
        setIcon(weatherIcons.Atmosphere);
        break;
      case weatherId === 800:
        setIcon(weatherIcons.Clear);
        break;
      case weatherId >= 801 && weatherId <= 804:
        setIcon(weatherIcons.Clouds);
        break;
      default: setIcon(weatherIcons.Clouds);
    }
  }
 
  // const dateBuilder = (d) => {

  //   let date = new Date();
  //   date.toLocaleString(d.sys.country, {timeZone: d.timezone});
  //   console.log(date);
  //   return`${date}`
  // }

  let handleChange = (e)=>{
    setQuery(e.target.value);
  }
  return (
    <div className={(typeof weather.main != "undefined") ? ((weather.main.temp > 20) ? 
    'app warm' : 'app') : 'app'}>
     <main className="w-75">
       <div className="search-box">
         <input
         type="text"
         className="search-bar"
         placeholder= "Search..."
         onChange={handleChange}
         value={query}
         onKeyPress={search} //wywołaj search
         >
         </input>
       </div>
       {(typeof weather.main != "undefined") ? (
        <section className="">
          <div className="location-box">
            <div className="location">{weather.name}, {weather.sys.country}</div>
            {//<div className="date">{weather.dt}</div>  //tutaj trzeba zmienić bo pokazuje złą dadę
            }
          </div>
          <div className="weather-box">
            <div className="row justify-content-center temp">
              <header className="fw-bold text-white col">{Math.round(weather.main.temp)}&deg;c</header>
              
            </div>
            <div className="row justify-content-center temp-small text-white mb-4">  
              <h6 className="col">
                {Math.round(weather.main.temp_min)}&deg;c/{Math.round(weather.main.temp_max)}&deg;c 
                Feels like {Math.round(weather.main.feels_like)}&deg;c
              </h6>
              {/* <h1 className="col">{Math.round(weather.main.temp_max)}&deg;c</h1> */}
            </div>
            <div className="weather">
            <h1 className="icon text-white col"> {icon}</h1>
            <h1 className="fw-bold text-white pb-4">{weather.weather[0].main}</h1>
            </div>
          </div>
        </section>
       ) : ('')}
     </main>
    </div>
  );
}

export default App;
